package li.wohl.algodat.generate;

import java.util.Random;

public class DataGenerator {

	public int[] generateDataArray(int size) {
		int[] data = new int[size];

		for (int i = 0; i < size; i++) {
			Random random = new Random();
			int number = random.nextInt();
			data[i] = number;
		}
		return data;
	}

	public int[] generateDataArray(int size, int min, int max) {
		int[] data = new int[size];

		for (int i = 0; i < size; i++) {
			Random random = new Random();
			int number = random.nextInt((max - min) + 1) + min;
			data[i] = number;
		}
		return data;
	}

	public void printArray(int[] data) {
		for (int i = 0; i < data.length; i++) {
			System.out.println(data[i]);
		}
	}
}
