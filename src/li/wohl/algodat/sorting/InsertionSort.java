package li.wohl.algodat.sorting;

public class InsertionSort implements Sorter {

	@Override
	public int[] sort(int[] sortieren) {

		int temp;
		
		for (int i = 1; i < sortieren.length; i++) {
			
			temp = sortieren[i];
			int j = i;
			
			while (j > 0 && sortieren[j - 1] > temp) {
				
				sortieren[j] = sortieren[j - 1];
				j--;
			}
			
			sortieren[j] = temp;
		}
		
		return sortieren;
	}
}
