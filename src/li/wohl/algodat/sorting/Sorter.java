package li.wohl.algodat.sorting;

public interface Sorter {
	public int[] sort(int[] sortieren);
}