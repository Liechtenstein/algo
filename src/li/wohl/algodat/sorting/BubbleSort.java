package li.wohl.algodat.sorting;

public class BubbleSort implements Sorter {

	@Override
	public int[] sort(int[] sortieren) {
		
		int temp;
		for (int i = 1; i < sortieren.length; i++) {
			
			for (int j = 0; j < sortieren.length - i; j++) {
				
				if (sortieren[j] > sortieren[j + 1]) {
					temp = sortieren[j];
					sortieren[j] = sortieren[j + 1];
					sortieren[j + 1] = temp;
				}

			}
		}
		return sortieren;
	}

}
