package li.wohl.algodat.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import li.wohl.algodat.generate.DataGenerator;
import li.wohl.algodat.sorting.SelectionSort;
import li.wohl.algodat.sorting.Sorter;

public class SelectionSort_Test {

	private DataGenerator classUnderTest;

	@Before
	public void setUp() throws Exception {
		classUnderTest = new DataGenerator();
	}

	@Test
	public void testSelectionSort() {
		
		int[] unsorted = classUnderTest.generateDataArray(10, 0, 1000);
		
		Sorter is = new SelectionSort();
		
		int[] sorted = is.sort(unsorted);
		
		System.out.println("*** Selection Sort ***");
			
		for (int i = 0; i < sorted.length - 1; i++) {
			
			System.out.println(i + ". Nummer: " + sorted[i]);

			if (sorted[i] > sorted[i + 1]) {
				fail("Nicht sortiert");
			}
		}
		
		assertTrue(true);
	}
}
