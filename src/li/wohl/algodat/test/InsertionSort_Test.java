package li.wohl.algodat.test;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import li.wohl.algodat.generate.DataGenerator;
import li.wohl.algodat.sorting.InsertionSort;
import li.wohl.algodat.sorting.Sorter;

public class InsertionSort_Test {

	private DataGenerator classUnderTest;

	@Before
	public void setUp() throws Exception {
		classUnderTest = new DataGenerator();
	}

	@Test
	public void testInsertionSort() {
		
		int[] unsorted = classUnderTest.generateDataArray(10, 0, 1000);
		
		Sorter is = new InsertionSort();
		
		int[] sortieren = is.sort(unsorted);

		System.out.println("*** Insertion Sort ***");
		
		for (int i = 0; i < sortieren.length - 1; i++) {
			
			System.out.println(i + ". Nummer: " + sortieren[i]);

			if (sortieren[i] > sortieren[i + 1]) {
				fail("Nicht sortiert");
			}
		}
		
		assertTrue(true);
	}
}
