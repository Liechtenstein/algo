package li.wohl.algodat.test;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import li.wohl.algodat.generate.DataGenerator;
import li.wohl.algodat.sorting.BubbleSort;
import li.wohl.algodat.sorting.Sorter;

public class BubbleSort_Test{

	private DataGenerator classUnderTest;
	
	@Before
	public void setUp() throws Exception {
		classUnderTest = new DataGenerator();
	}

	@Test
	public void testBubbleSort() {
		
		int[] unsorted = classUnderTest.generateDataArray(10, 0, 1000);
		
		Sorter is = new BubbleSort();
		
		int[] sortieren = is.sort(unsorted);
		
		System.out.println("*** Bubble Sort ***");
		
		for (int i = 0; i<sortieren.length-1; i++) {
			
			System.out.println(i+". Nummer:   " + sortieren[i]);
			
			if(sortieren[i]> sortieren[i + 1]) {
				fail("nicht sortiert");
			}
		}
		
		assertTrue(true);
	}
}
