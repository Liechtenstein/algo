package li.wohl.algodat.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import li.wohl.algodat.basealg.BaseAlgorithms;

public class BaseAlgorithmsTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testRevert() {
		BaseAlgorithms revert = new BaseAlgorithms();
		int[] data = {3,7,5,9,1};
		assertArrayEquals(new int[] {1,9,5,7,3}, revert.revert(data));
	}

	@Test
	public void testMax() {
		BaseAlgorithms ba = new BaseAlgorithms();
		int[] data = {3,7,5,9,1};
		assertEquals(9, ba.max(data));
	}

	@Test
	public void testMin() {
		BaseAlgorithms ba = new BaseAlgorithms();
		int[] data = {3,7,5,9,1};
		assertEquals(1, ba.min(data));
	}

}
