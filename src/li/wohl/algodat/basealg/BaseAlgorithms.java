package li.wohl.algodat.basealg;

public class BaseAlgorithms {

	public int[] revert(int[] data) {
		int[] reverted = new int[data.length];
		for (int i = 1; i <= data.length; i++) {
			reverted[data.length - i] = data[i-1];
		}
		return reverted;
	}
	
	public int max(int[] data) {
		int m = Integer.MIN_VALUE;
		for (int a : data ) {
			m = Math.max(m, a);
		}
		return m;
	}
	
	public int min( int[] data) {
		int m = Integer.MAX_VALUE;
        for (int a : data) {
            m = Math.min(m, a);
        }
        return m;
	}
	
}
